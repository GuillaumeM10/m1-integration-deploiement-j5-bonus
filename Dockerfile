FROM node:20
WORKDIR /app
COPY package*.json ./
RUN npm install
RUN npm install -g newman
COPY ./ /app/
EXPOSE 3000
ENV PORT=3000
CMD ["node", "/app/src/app.js"]
