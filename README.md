# Usefull commands

```
# Login to gitlab registry
docker login registry.gitlab.com


# Build the image from gitlab registry
docker run -d -p 8090:80 --name ynov registry.gitlab.com/guillaumem10/name
## http://localhost:8090/

# Exectute commandes in image
docker exec -it ynov /bin/bash

# Stop the container
docker stop ynov

# Remove the container
docker rm ynov

# Remove the image
docker rmi registry.gitlab.com/guillaumem10/name

# Push the image to gitlab registry
docker push registry.gitlab.com/guillaumem10/name

# Build the image from dockerfile
docker build -t registry.gitlab.com/guillaumem10/name .

newman run ./tests/TP.postman_collection.json --env-var "URL_APP=http://localhost:3000"

```

## Infos

- Packages & registries > Container Registry
