.PHONY: up
up:
	docker-compose up -d

.PHONY: down
down:
	docker-compose down

.PHONY: reset
reset:
	docker stop $$(docker ps -a -q) \
	&& docker rm $$(docker ps -a -q) \
	&& docker rmi $$(docker images -q) \
	&& docker volume rm $$(docker volume ls -q) \
	&& docker system prune -a -f

.PHONY: run
run:
	docker run -d -p 8090:80 --name ynov registry.gitlab.com/guillaumem10/

.PHONY: localtest
localtest:
	newman run ./tests/TP.postman_collection.json --env-var "URL_APP=http://localhost:3000"